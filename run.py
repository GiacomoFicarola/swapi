import os
import uvicorn

# This is the entry point for the FastAPI application
if __name__ == '__main__':
    port = int(os.environ.get("_LOCALHOST_DEBUG_PORT", 8080))
    uvicorn.run('app.main:app', host='localhost', port=port, reload=True)