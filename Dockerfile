# Select image
FROM python:3.12

# Copy all files from the current directory to the container
COPY . /app

# set the working directory
WORKDIR /app

# execute the command
CMD ["python", "run.py"]
