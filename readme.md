# This is a base start for your FastAPI project!

## Brief introduction to folder structure:

This is just a template. You can edit it regarding your target goal. You have to follow this structure as a guideline but any project has its requirements and functionalities.

---

### Structure:

-   **config**: used for your configurations like:
    -   environment variable read
    -   secrets data read
    -   ecc
-   **service**: Controller pillar of MVC. Here there is all the business logics and it is divided into 2 sub folders:
    -   **public**: contains the business logics that should be related to your public endpoints
-   **model**: Model pillar of MVC. Contains all models, DAO, DTO ecc. There are 2 sub folders:
    -   **daos**: contains DBs logics and database data manipulation.
    -   **schemas**: data representation useful for business logics, input/output models.
    -   **db_schemas**: database data representation.
-   **router**: View pillar or MVC. Contains all endpoint divided by functionalities. There are 2 sub folders:
    -   **public**: used for public endpoints
-   **utils**: contains logics that could be re-used. This folder could be (or better, must be) replaces by a private library (PyPi Server or, in GCP, Artifact Registry) in order to share the code among micro-services.

The **v1** folder is used in order to version your code. In this way you can deploy a new version (v2) and at the same time maintain also the older one.

The v1 folder (v2, v3 ecc) is a sub-folder of **app**. This last folder, **app** should be the unique folder you have to deploy. It contains your application code.

---

### public service

A public endpoint call is a made by the frontend, for example. It is an external call for which we could be interested to the caller context (who is he):

-   we should retrieve caller information from Header A
-   we should parse data in way A
-   we should take attention to something
-   ecc

A public endpoint must have an authorizations step (RBAC for example. The role should be retrieved from the caller context)

In this way I create one flows:

-   public_router -> public_service -> base_service

---

### Utils

I suggest you to create utils folder internally to the repository for useful codes

For piece of codes that must be sharable between services (in a micro-service architecture, for example) I suggest you to create a "common library" stored in a PyPi Server or, in GCP, in an Artifact Registry, in order to make this code accessible by all services

---

### Database

I use a custom class that mock a DB library. I use this approach because I can not know what DB you will use:

-   MySQL (with ORM or not)
-   NoSQL

I declared a custom class only to show you how to use it within the structure. The DAOs are responsible for the DB management.
DAOs are not DBs. DAOs **uses** the DBs. The idea is that if you change DB, you have to do some adjustment only into your DAOs.
Controllers and routers should not be changed.

Also in this case, I suggest you to create a "common library" stored in a PyPi Server or, in GCP, in an Artifact Registry, in order to make this code accessible by all services

---

### Authentication

An example of authentication is provided by `get_user_context` which checks for a field `X-Endpoint-Api-Userinfo` in the request header

If endpoint should not be authenticated, simply omit the code related to `UserContext`

---

### How to

#### PyCharm

In order to run this template you have to:

-   Mark the **app** folder as Source Root
-   Create your virtual environment
    -   venv
    -   docker-compose
    -   other
-   Open a new terminal, to activate your virtual environment
-   Install requirements contained into **requirements.txt**
    -   `pip install -r requirements.txt`
-   Run the **run.py** using the environment variable contained into **.env.default**
    -   I suggest you the duplicate the .env.default and use a .env.development in your local environment.
    -   The .env.default should be taken aligned in order to guarantee to other devs to operate correctly.
    -   With your browser, go to _localhost:8080/docs_

#### VSCode

In order to run this template you have to:

-   Create your virtual environment
    -   venv
    -   docker-compose
    -   other
-   Open a new terminal, to activate your virtual environment
-   Install requirements contained into **requirements.txt**
    -   `pip install -r requirements.txt`
-   Go to "Launch with Debug" section of VSCode
-   Create a new "FastAPI" config file in execution menu
-   On your launch.json, add the following configurations:
    -   ```
        "console": "integratedTerminal",
        "cwd": "${fileDirname}",
        "env": {
          "PYTHONPATH": "${workspaceFolder}/app;${workspaceFolder}/test"
        },
        "envFile": "${workspaceFolder}/.env.development",
        ```
    ```
    * env contains the folders to be known for your application
    * envFile is the name of .env to consider (you can have multiple .env, for example 1 for each branch)
    ```

An example of launch.json file would be

```
  {
    // Usare IntelliSense per informazioni sui possibili attributi.
    // Al passaggio del mouse vengono visualizzate le descrizioni degli attributi esistenti.
    // Per altre informazioni, visitare: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [

        {
            "name": "Python: FastAPI",
            "type": "python",
            "request": "launch",
            "module": "uvicorn",
            "args": [
                "main:app"
            ],
            "jinja": true,
            "justMyCode": true,
            "console": "integratedTerminal",
            "cwd": "${fileDirname}",
            "env": {
                "PYTHONPATH": "${workspaceFolder}/app;${workspaceFolder}/test"
            },
            "envFile": "${workspaceFolder}/.env.development",
        }
    ]
}
```

#### How to run Unit Tests in VSCode

In order to run unit tests, you need to:

-   follow the previous steps described in How To (VSCode),
-   update your launch.json adding the following configuration:

```
{
            "name": "Python: tests",
            "type": "python",
            "request": "launch",
            "program": "${workspaceFolder}/test/unit_test.py",
            "console": "integratedTerminal",
            "cwd": "${workspaceFolder}/test",
            "env": {
                "PYTHONPATH": "${workspaceFolder};${workspaceFolder}/app;${workspaceFolder}/test",
                "X-ENDPOINT-API-USERINFO": "eyJ1c2VyIjoiMTIzNDU2Nzg5IiwgImVtYWlsIjogImxvcmVuem8ifQ==",
            },
        }
```

### PLEASE NOTE: At the moment, testing does not support .env files (even using envFile config seems to be ignored), so all your configurations needs to be placed in the `env` section.
