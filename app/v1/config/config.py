from functools import lru_cache
from pydantic import BaseSettings

# Define a model for your configuration.
class Settings(BaseSettings):
    env: str = 'local-env'
    port: int = 8080
    service_name: str = 'my-service'
    short_sha: str = 'local-sha'

    # Optional
    header: str = "X-Endpoint-Api-Userinfo"


@lru_cache()
def get_config() -> Settings:
    return Settings()
