from starlette import status
from v1.utils.responses import HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR, HTTP_422_UNPROCESSABLE_ENTITY

# Responses for the endpoints in the router
get_all_responses: dict = {
    status.HTTP_200_OK: HTTP_200_OK.dict(),
    status.HTTP_500_INTERNAL_SERVER_ERROR: HTTP_500_INTERNAL_SERVER_ERROR.dict()
}

get_one_responses: dict = {
    status.HTTP_200_OK: HTTP_200_OK.dict(),
    status.HTTP_500_INTERNAL_SERVER_ERROR: HTTP_500_INTERNAL_SERVER_ERROR.dict()
}

delete_all_responses: dict = {
    status.HTTP_200_OK: HTTP_200_OK.dict(),
    status.HTTP_500_INTERNAL_SERVER_ERROR: HTTP_500_INTERNAL_SERVER_ERROR.dict()
}

post_responses: dict = {
    status.HTTP_200_OK: HTTP_200_OK.dict(),
    status.HTTP_500_INTERNAL_SERVER_ERROR: HTTP_500_INTERNAL_SERVER_ERROR.dict(),
    status.HTTP_422_UNPROCESSABLE_ENTITY: HTTP_422_UNPROCESSABLE_ENTITY.dict()
}

update_responses: dict = {
    status.HTTP_200_OK: HTTP_200_OK.dict(),
    status.HTTP_500_INTERNAL_SERVER_ERROR: HTTP_500_INTERNAL_SERVER_ERROR.dict(),
    status.HTTP_422_UNPROCESSABLE_ENTITY: HTTP_422_UNPROCESSABLE_ENTITY.dict()
}
