from typing import List
from fastapi import APIRouter, Depends
from v1.model.schemas.schema import PeopleSchema
from v1.service.public.people_service import PeopleService
from v1.router.endpoint_responses import get_all_responses

# This router contains the endpoints people for obtaining the people data.
router = APIRouter(
    tags=['people'],
    # prefix="/people"
)


@router.get("/people", response_model=List[PeopleSchema], responses=get_all_responses)
def get_all(
        people_service: PeopleService = Depends()
) -> list[PeopleSchema]:
    """
    Return all schemas
    """
    return people_service.get_all()
