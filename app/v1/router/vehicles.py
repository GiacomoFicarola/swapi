from typing import List
from fastapi import APIRouter, Depends
from v1.router.endpoint_responses import get_all_responses
from v1.service.public.vehicles_service import VehiclesService
from v1.model.schemas.schema import VehicleSchema

# This router contains the endpoints vehicles for obtaining the vehicles data.
router = APIRouter(
    tags=['vehicles']
)


@router.get("/vehicles", response_model=List[VehicleSchema], responses=get_all_responses)
def get_all(
        vehicles_service: VehiclesService = Depends()
) -> list[VehicleSchema]:
    """
    Return all schemas
    """
    return vehicles_service.get_all()
