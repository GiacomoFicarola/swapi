from typing import List
from fastapi import APIRouter, Depends
from v1.model.schemas.schema import PlanetSchema
from v1.router.endpoint_responses import get_all_responses
from v1.service.public.planet_service import PlanetService

# This router contains the endpoints planet for obtaining the planet data.
router = APIRouter(
    tags=['planet']
)


@router.get("/planets", response_model=List[PlanetSchema], responses=get_all_responses)
def get_all(
        planet_service: PlanetService = Depends()
) -> list[PlanetSchema]:
    """
    Return all schemas
    """
    return planet_service.get_all()
