from datetime import datetime
from fastapi import APIRouter, Depends
from v1.config import Settings, get_config

# This router contains the endpoints for the health check of the API status.
router = APIRouter(
    tags=['health']
)


@router.get("/health")
def health():
    return {
        "message": "healthy"
    }


@router.get("/health-check")
def health_check(config: Settings = Depends(get_config)):
    return {
        "message": "healthy",
        "version": config.short_sha,
        "time": datetime.now()
    }
