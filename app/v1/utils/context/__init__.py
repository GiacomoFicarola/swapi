import json

from fastapi import HTTPException, Header
from pydantic import BaseModel
from starlette import status
from v1.config import get_config, Settings
from v1.utils import decode_base64

_config: Settings = get_config()


class UserContext(BaseModel):
    user: str
    email: str


def fake_user():
    return UserContext(user="fake", email="fake")


def get_user_context(token: str = Header(None, alias=_config.header)) -> UserContext:
    if not token:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"You must be authenticated!",
            internal_detail=f"No valid token is provided! Please provide a public header."
        )
    user_context_data: UserContext = UserContext(
        **json.loads(decode_base64(token))
    )
    return user_context_data
