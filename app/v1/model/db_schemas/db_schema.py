from datetime import datetime
from typing import Optional
from pydantic import BaseModel
from sqlalchemy import Column, String, Integer, ForeignKey, Table
from sqlalchemy.orm import declarative_base, relationship
from v1.utils import to_camel_case


# class AuditField(BaseModel):
#     created_at: datetime = datetime.now()
#     updated_at: Optional[datetime] = None
#     created_by: str
#     updated_by: Optional[str] = None

#     class Config:
#         allow_population_by_field_name = True
#         alias_generator = to_camel_case


# Define a model for your DB data.
# I use a pydantic model but if you can use an ORM schema model for MySQL or whatever you want
# Define a declarative base
Base = declarative_base()

people_vehicles = Table('people_vehicles', Base.metadata,
                        Column('people_id', Integer, ForeignKey('sith.people.id')),
                        Column('vehicle_id', Integer, ForeignKey('sith.vehicles.id')),
                        schema='sith'
                        )

# Define a model for your data IN. Only fields declared will be taken into account.
class People(Base):
    __tablename__ = 'people'
    __table_args__ = {'schema': 'sith'}

    id = Column(Integer, primary_key=True)
    birth_year = Column(String(50))
    eye_color = Column(String(50))
    gender = Column(String(50))
    hair_color = Column(String(50))
    height = Column(String(50))
    mass = Column(String(50))
    name = Column(String(100))
    skin_color = Column(String(50))
    created = Column(String(50))
    url = Column(String(100))

    vehicles = relationship("Vehicle", secondary=people_vehicles, back_populates="people")

    homeworld_id = Column(Integer, ForeignKey('sith.planets.id'))
    homeworld = relationship("Planets", back_populates="residents")
    
    # Define a model to return data to the caller.
    def dict(self, include_vehicles=False):
        data = {
            'id': self.id,
            'birth_year': self.birth_year,
            'eye_color': self.eye_color,
            'gender': self.gender,
            'hair_color': self.hair_color,
            'height': self.height,
            'mass': self.mass,
            'name': self.name,
            'skin_color': self.skin_color,
            'created': self.created,
            'url': self.url,
            'homeworld': self.homeworld.dict() if self.homeworld else None,
        }
        if include_vehicles:
            data['vehicles'] = [vehicle.dict() for vehicle in self.vehicles]
        return data

# Define a model VehicleSchema to return data to the caller.
class Vehicle(Base):
    __tablename__ = 'vehicles'
    __table_args__ = {'schema': 'sith'}

    id = Column(Integer, primary_key=True)
    cargo_capacity = Column(String(50))
    consumables = Column(String(50))
    cost_in_credits = Column(String(50))
    created = Column(String(50))
    crew = Column(String(50))
    length = Column(String(50))
    manufacturer = Column(String(100))
    max_atmosphering_speed = Column(String(50))
    model = Column(String(100))
    name = Column(String(100))
    passengers = Column(String(50))
    url = Column(String(100))
    vehicle_class = Column(String(50))

    people = relationship("People", secondary=people_vehicles, back_populates="vehicles")

    # Define a model to return data to the caller.
    def dict(self, include_people=False):
        data = {
            'id': self.id,
            'cargo_capacity': self.cargo_capacity,
            'consumables': self.consumables,
            'cost_in_credits': self.cost_in_credits,
            'created': self.created,
            'crew': self.crew,
            'length': self.length,
            'manufacturer': self.manufacturer,
            'max_atmosphering_speed': self.max_atmosphering_speed,
            'model': self.model,
            'name': self.name,
            'passengers': self.passengers,
            'url': self.url,
            'vehicle_class': self.vehicle_class,
        }
        if include_people:
            data['pilots'] = [person.dict() for person in self.people]
        return data

# Define a model PlanetSchema to return data to the caller.
class Planets(Base):
    __tablename__ = 'planets'
    __table_args__ = {'schema': 'sith'}

    id = Column(Integer, primary_key=True)
    climate = Column(String(50))
    diameter = Column(String(50))
    gravity = Column(String(50))
    name = Column(String(100))
    orbital_period = Column(String(50))
    population = Column(String(50))
    rotation_period = Column(String(50))
    surface_water = Column(String(50))
    terrain = Column(String(100))
    url = Column(String(100))

    residents = relationship("People", back_populates="homeworld")

    # Define a model to return data to the caller.
    def dict(self):
        return {
            'id': self.id,
            'climate': self.climate,
            'diameter': self.diameter,
            'gravity': self.gravity,
            'name': self.name,
            'orbital_period': self.orbital_period,
            'population': self.population,
            'rotation_period': self.rotation_period,
            'surface_water': self.surface_water,
            'terrain': self.terrain,
            'url': self.url
        }
