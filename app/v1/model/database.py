from datetime import datetime
from select import select
from typing import List
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from v1.model.db_schemas.db_schema import People, Base


class YourDbEntityClient:
    pass

    @staticmethod
    def get(target_id: str) -> People:
        data_out: People = People(
            message=f"This is your message retrieved from DB. Id {target_id}",
            message_only_for_post_and_get="This is your post message retrieved from DB",
            created_by="Me",
            updated_by="You",
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        return data_out

    @staticmethod
    def get_all() -> List[People]:
        data_out: List[People] = select(People)
        return data_out

    @staticmethod
    def post(data: People):
        return f"You are inserting |-> {data}"

    @staticmethod
    def update(data: People):
        return f"Your are updating |-> {data}"


# Create the connection to the database
def get_db():
    DB_URL = 'postgresql://username:password@server:porta/postgres'


    engine = create_engine(DB_URL)
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()
    return session
