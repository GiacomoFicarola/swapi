from typing import List
from fastapi import Depends
from sqlalchemy.orm import joinedload
from v1.model.database import get_db, YourDbEntityClient
from v1.model.db_schemas.db_schema import Vehicle

# This class contains the methods to interact with the database for the vehicles.
class VehiclesDAO:

    def __init__(self, db: YourDbEntityClient = Depends(get_db)):
        self.db: YourDbEntityClient = db

    def get_all(self) -> List[Vehicle]:
        return self.db.query(Vehicle).options(joinedload(Vehicle.people)).all()
