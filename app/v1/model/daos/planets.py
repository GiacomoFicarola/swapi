from typing import List
from fastapi import Depends
from v1.model.database import get_db, YourDbEntityClient
from v1.model.db_schemas.db_schema import Planets

# This class contains the methods to interact with the database for the planets.
class PlanetsDAO:

    def __init__(self, db: YourDbEntityClient = Depends(get_db)):
        self.db: YourDbEntityClient = db

    def get_all(self) -> List[Planets]:
        return self.db.query(Planets).all()
