from typing import Optional
from pydantic import BaseModel
from v1.utils import to_camel_case


# # Define a model for your data IN. Only fields declared will be taken into account.
# class PostSchemaIn(BaseModel):
#     message: str
#     message_only_for_post_and_get: str
#     excluded_for_db_message: str

#     class Config:
#         allow_population_by_field_name = True
#         alias_generator = to_camel_case


# # Define a model for your data IN. Only fields declared will be taken into account.
# class UpdateSchemaIn(BaseModel):
#     message: str

#     class Config:
#         allow_population_by_field_name = True
#         alias_generator = to_camel_case


# # Define a model to return data to the caller.
# # Only the fields you want to return
# class GetSchemaOut(BaseModel):
#     message: str
#     message_only_for_post_and_get: str

#     class Config:
#         allow_population_by_field_name = True
#         alias_generator = to_camel_case

# Define a model VehicleSchema to return data to the caller.
class VehicleSchema(BaseModel):
    id: Optional[int]
    cargo_capacity: Optional[str]
    consumables: Optional[str]
    cost_in_credits: Optional[str]
    created: Optional[str]
    crew: Optional[str]
    length: Optional[str]
    manufacturer: Optional[str]
    max_atmosphering_speed: Optional[str]
    model: Optional[str]
    name: Optional[str]
    passengers: Optional[str]
    url: Optional[str]
    vehicle_class: Optional[str]
    pilots: Optional[list]

# Define a model PlanetSchema to return data to the caller.
class PlanetSchema(BaseModel):
    id: Optional[int]
    climate: Optional[str]
    diameter: Optional[str]
    gravity: Optional[str]
    name: Optional[str]
    orbital_period: Optional[str]
    population: Optional[str]
    rotation_period: Optional[str]
    surface_water: Optional[str]
    terrain: Optional[str]
    url: Optional[str]

# Define a model PeopleSchema to return data to the caller.
class PeopleSchema(BaseModel):
    id: Optional[int]
    birth_year: Optional[str]
    eye_color: Optional[str]
    gender: Optional[str]
    hair_color: Optional[str]
    height: Optional[str]
    mass: Optional[str]
    name: Optional[str]
    skin_color: Optional[str]
    created: Optional[str]
    url: Optional[str]
    homeworld: Optional[PlanetSchema]
    vehicles: Optional[list]
