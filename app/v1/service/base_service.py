import logging
from typing import List
from fastapi import Depends
from v1.config import Settings, get_config
from v1.model.daos.people import PeopleDAO
from v1.model.db_schemas.db_schema import People
from v1.model.schemas.schema import PostSchemaIn, UpdateSchemaIn


# This service contains the business logic.
# This is extended by other services in order to allow code reusing without duplicate it.
class BaseService:

    def __init__(
            self,
            config: Settings = Depends(get_config),
            dao: PeopleDAO = Depends()
    ):
        self.config: Settings = config
        self.dao: PeopleDAO = dao
        self.logger: logging.Logger = logging.getLogger(config.service_name)

    def _get_all(self) -> List[People]:
        return self.dao.get_all()

    def _get(self, target_id: str) -> People:
        return self.dao.get(target_id=target_id)

    def _post(self, data: PostSchemaIn, created_by: str):
        res: str = self.dao.post(data=data, created_by=created_by)
        self.logger.info(res)

    def _update(self, target_id: str, data: UpdateSchemaIn, updated_by: str):
        res: str = self.dao.update(target_id=target_id, data=data, updated_by=updated_by)
        self.logger.info(res)

    def _delete_all(self, user: str):
        self.logger.info(f"{user} deleted all schemas.")
        self.dao.delete_all()
