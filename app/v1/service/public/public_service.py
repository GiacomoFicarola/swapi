from typing import List
from fastapi import Depends
from v1.config import Settings, get_config
from v1.model.daos.dao import DAO
from v1.model.schemas.schema import UpdateSchemaIn, PostSchemaIn, GetSchemaOut
from v1.model.db_schemas.db_schema import People
from v1.service.base_service import BaseService
from v1.utils.context import UserContext, fake_user


# This service contains the business logic.
class PublicService(BaseService):
    def __init__(
            self,
            config: Settings = Depends(get_config),
            dao: DAO = Depends(),
            #
            user_context: UserContext = Depends(fake_user)  # checks user **token**
    ) -> None:
        super().__init__(
            config=config,
            dao=dao
        )
        self.user_context: UserContext = user_context

    def get_all(self) -> List[GetSchemaOut]:
        list_schemas: List[People] = self._get_all()
        return [GetSchemaOut(**schema.dict(True)) for schema in list_schemas]

    def get(self, target_id) -> GetSchemaOut:
        schema: People = self._get(target_id=target_id)
        return GetSchemaOut(**schema.dict())

    def post(self, data: PostSchemaIn):
        created_by: str = self.user_context.user
        self._post(data=data, created_by=created_by)

    def update(self, target_id: str, data: UpdateSchemaIn):
        updated_by: str = self.user_context.user
        self._update(target_id=target_id, data=data, updated_by=updated_by)
