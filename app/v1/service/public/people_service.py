from typing import List
from fastapi import Depends
from v1.config import Settings, get_config
from v1.model.daos.people import PeopleDAO
from v1.model.schemas.schema import PeopleSchema
from v1.model.db_schemas.db_schema import People
from v1.service.base_service import BaseService
from v1.utils.context import UserContext, fake_user


# This service contains the business logic.
class PeopleService(BaseService):
    def __init__(
            self,
            config: Settings = Depends(get_config),
            dao: PeopleDAO = Depends(),
            #
            user_context: UserContext = Depends(fake_user)  # checks user **token**
    ) -> None:
        super().__init__(
            config=config,
            dao=dao
        )
        self.user_context: UserContext = user_context

    def get_all(self) -> List[PeopleSchema]:
        list_schemas: List[People] = self._get_all()
        return [PeopleSchema(**schema.dict(True)) for schema in list_schemas]
