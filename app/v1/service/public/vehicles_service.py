from typing import List
from fastapi import Depends
from v1.config import Settings, get_config
from v1.model.daos.vehicles import VehiclesDAO
from v1.model.schemas.schema import VehicleSchema
from v1.model.db_schemas.db_schema import Vehicle
from v1.service.base_service import BaseService
from v1.utils.context import UserContext, fake_user


# This service contains the business logic.
class VehiclesService(BaseService):
    def __init__(
            self,
            config: Settings = Depends(get_config),
            dao: VehiclesDAO = Depends(),
            #
            user_context: UserContext = Depends(fake_user)  # checks user **token**
    ) -> None:
        super().__init__(
            config=config,
            dao=dao
        )
        self.user_context: UserContext = user_context

    def get_all(self) -> List[VehicleSchema]:
        list_schemas: List[Vehicle] = self._get_all()
        return [VehicleSchema(**schema.dict(True)) for schema in list_schemas]
