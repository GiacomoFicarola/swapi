from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from starlette import status
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from v1.config import get_config
from v1.router import people, planet, vehicles

# this is the entry point for the FastAPI application contains the main function that creates the FastAPI app
def main() -> FastAPI:
    config = get_config()
    title = f"{config.env} - {config.service_name}"

    # Middleware to allow CORS
    middleware = [
        Middleware(
            CORSMiddleware,
            allow_origins=['*'],
            allow_credentials=True,
            allow_methods=['*'],
            allow_headers=['*']
        )
    ]

    # Initialize FastAPI app
    app: FastAPI = FastAPI(title=title, version=config.short_sha, debug=False, middleware=middleware)

    # Routers
    # /
    # included all routers in the app
    app.include_router(people.router)
    app.include_router(planet.router)
    app.include_router(vehicles.router)

    @app.exception_handler(Exception)
    async def exception_handler(request: Request, exc: Exception):
        status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        print(f"{request.method} {request.url} {status_code} : {repr(exc)}")
        return JSONResponse({'detail': 'Internal Server Error'}, status_code=status_code)

    return app


app: FastAPI = main()
